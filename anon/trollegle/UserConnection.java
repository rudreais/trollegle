package anon.trollegle;

import anon.trollegle.JsonValue.Kind;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Proxy;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.function.UnaryOperator;

import static anon.trollegle.Util.fillHeaders;
import static anon.trollegle.Util.t;

/**
 * A single chat session on Omegle.
 */
public class UserConnection {
    
    @Persistent
    private String randid = Util.makeRandid();
    @Persistent
    private String
        id = "", // clientID assigned by Omegle
        question; // Question shown to user
    /** Receives all of the session's events */
    private Callback<? super UserConnection> callback;
    @Persistent
    private int msgCount;
    @Persistent
    protected int server = chooseFront();
    @Persistent
    private boolean
        created, // Whether the /start call was successful
        connected, // Whether the chat is currently open
        lived, // Whether the chat was ever open
        questionMode, // Whether the chat is to be started with wantsspy=1
        dummy; // Whether the user is a dummy (without an actual Omegle connection)
    private boolean
        done, // Whether the chat has ended. The two threads exit once this is set.
        typing, seesTyping;
    /** Threads that handle remote (from Omegle) and local (from the room or other application) events */
    private Thread remotePoller, localPoller;
    /** Contains messages to be sent to the remote user */
    private final BlockingQueue<String> tellQueue = new LinkedBlockingDeque<>();
    /** Time of last message from the remote user */
    @Persistent
    private long lastActive = System.currentTimeMillis();
    /** Time at which the chat was opened, or Long.MAX_VALUE if never */
    @Persistent
    private long birth = Long.MAX_VALUE;
    private static final ProxyConfig NO_PROXY = new ProxyConfig(Proxy.NO_PROXY);
    private ProxyConfig proxy = NO_PROXY;
    /** Whether the above proxy is ignored in favour of the direct connection for everything after the /start call. */
    @Persistent
    private boolean moveProxy;
    /** Common interests between us and the remote user, as sent by Omegle */
    @Persistent
    private String[] commonLikes;
    @Persistent
    private String commonLikesFlat;
    /** Last message said by the remote user */
    @Persistent
    private String lastLine;
    private static final String LANG = "en";
    /** Language to report to Omegle. For some languages, this makes Omegle prefer pairing us with users of the same language. */
    @Persistent
    private String lang = LANG;
    /** For connections where Omegle requires a captcha to be solved, the site key to pass to Google. */
    @Persistent
    private String captchaSiteKey;
    
    /**
     * The type of chat. PULSE is a special case of TEXT.
     * This actually belongs in MultiUser but stays here in case someone depends on it.
     */
    public enum JoinType { TEXT, QUESTION, PULSE, BARE };
    
    /** The interests given to Omegle in the /start call */
    @Persistent
    private String[] topicsArray = {};
    @Persistent
    private String topics;

    /** Whether to print debugging information */
    static boolean verbose;

    public UserConnection(Callback<? super UserConnection> callback) {
        this.callback = callback;
    }
    
    @Deprecated
    public UserConnection fill(boolean ignored, boolean questionMode, ProxyConfig proxy) {
        return fill(questionMode, proxy);
    }

    /** Sets the question mode status and proxy */
    public UserConnection fill(boolean questionMode, ProxyConfig proxy) {
        checkLate();
        this.questionMode = questionMode;
        this.proxy = proxy;
        this.randid = proxy.getRandid();
        return this;
    }
    
    /** Sets the language to be reported to Omegle */
    public UserConnection withLang(String lang) {
        checkLate();
        this.lang = lang;
        return this;
    }
    
    protected void checkLate() {
        if (created || connected || done || lived)
            throw new RuntimeException("already set in stone");
    }
    
    protected void stopLookingForCommonLikes() {
        if (connected || done || lived)
            return;
        topics = "";
        topicsArray = new String[0];
        sendAction("stoplookingforcommonlikes", null);
    }
    
    // Hooks to modify any events before passing them to the callback
    protected void callback(String action, String data) {
        callback.callback(this, action, data);
    }
    protected void callback(String action, int data) {
        callback.callback(this, action, data);
    }
    
    /** Returns a string that can be used to identify the connection */
    public String getDisplayNick() { return id; }
    
    public int getMsgCount() { return msgCount; }
    protected void setMsgCount(int c) { msgCount = c; }
    /** Returns the clientID assigned by Omegle */
    public String getID() { return id; }
    public String getRandid() { return randid; }
    /** Returns when the remote user last sent a message */
    public long lastActive() { return lastActive; }
    /** Returns the last message sent by the remote user */
    public String getLastLine() { return lastLine; }

    /** Returns the time passed since the remote user last sent a message */
    public long idleFor() {
        return System.currentTimeMillis() - lastActive;
    }

    /** Returns the time passed since the chat started */
    public long age() {
        return System.currentTimeMillis() - birth;
    }
    
    /** Returns the proxy used to start the chat */
    public ProxyConfig getProxy() { return proxy; }
    /** Sets the proxy. This is likely to end the connection. */
    protected void setProxy(ProxyConfig proxy) { this.proxy = proxy; }
    /** Returns the question shown to us, if any */
    public String getQuestion() { return question; }
    /** Returns the langage reported to Omegle */
    public String getLang() { return lang; }
    /** Returns whether the /start call was successful */
    public boolean isCreated() { return created; }
    /** Returns whether the chat is open */
    public boolean isConnected() { return connected; }
    /** For connections where Omegle requires a captcha to be solved, returns the site key to pass to Google. */
    public String getCaptchaSiteKey() { return captchaSiteKey; }
    /**
     * Sets whether to ignore the proxy after the /start call.
     * Setting this to true is likely to end the connection.
     */
    protected void setMoveProxy(boolean moveProxy) { this.moveProxy = moveProxy; }

    /**
     * Returns whether the remote user is ready to accept messages.
     * Equivalent to isConnected by default.
     */
    public boolean isReady() {
        return connected;
    }

    /** Returns whether the chat was ever opened */
    public boolean didLive() {
        return lived;
    }

    public boolean isQuestionMode() { return questionMode; }
    public boolean isTyping() { return typing; }
    public boolean isDummy() { return dummy; }
    
    /** Returns the interests used to start the chat as a URL parameter */
    public String getTopics() {
        return topics;
    }

    /** Returns the interests used to start the chat */
    public String[] getTopicsArray() {
        return topicsArray.clone();
    }
    
    /**
     * Returns whether this connection is currently listening on the given topic.
     * This is only the case after /start has been called with the topic, before a remote user is found.
     */
    public boolean isListeningOnTopic(String topic) {
        return !connected && !done && Util.contains(topicsArray, topic);
    }
    
    /** Sets the interests to give to Omegle when starting the chat. */
    public UserConnection withTopics(String... topics) {
        checkLate();
        if (topics != null) {
            topicsArray = topics.clone();
            this.topics = topicsToString(topicsArray);
        }
        return this;
    }
    
    static String topicsToString(String[] topics) {
        if (topics.length == 0)
            return "";
        return "&topics=" + Util.urlEncode(JsonValue.wrap(topics).toString());
    }
    
    /** Whether to send messages queued before a remote user was found */
    protected boolean queueBeforeConnect() { return false; }
    
    @Override
    public String toString() {
        return getNickAndTrivia();
    }
    
    public String getNickAndTrivia() {
        return getDisplayNick() + " (" + getTrivia() + ")";
    }
    
    public String getTrivia() {
        return getQuestionOrTopics() + (lang == LANG ? ", " : ", lang=" + lang + ", ") + getRelevantAge();
    }
    
    protected String getRelevantAge() {
        long age = age();
        if (age < 0)
            return t(created ? "waitingage" : "zombieage", Util.minSec(System.currentTimeMillis() - lastActive));
        return t("age", Util.minSec(age));
    }
    
    protected String getQuestionOrTopics() {
        if (questionMode)
            return question == null ? t("questionmode") : question;
        if (commonLikesFlat != null)
            return commonLikesFlat;
        if (topicsArray.length == 0)
            return t("textmode");
        if (topicsArray.length > 7)
            return String.join(", ", Arrays.copyOf(topicsArray, 7)) + " [...]";
        return String.join(", ", topicsArray);
    }

    /** Turns this object into a dummy, such that it never connects to Omegle */
    protected UserConnection dummy() {
        dummy = true;
        created = true;
        connected = true;
        lived = true;
        topics = "";
        topicsArray = new String[0];
        birth = System.currentTimeMillis();
        msgCount = 5;
        return this;
    }
    
    /** Called once /start succeeds. By default, only logs a debug message. */
    protected void startSent() {
        logVerbose("Connection established for " + getDisplayNick());
    }
    
    /** Returns the proxy used for everything after the /start call. */
    public ProxyConfig effectiveProxy() {
        return moveProxy ? NO_PROXY : proxy;
    }
    
    /**
     * Returns whether the connection depends on its proxy being available.
     * This is used, for example, to decide whether a Tor instance can be told to switch circuits.
     */
    public boolean isUsingProxy() {
        return connected && !moveProxy && proxy.getProxy() != Proxy.NO_PROXY;
    }

    /**
     * Attempts to establish a connection to Omegle. Don't call this directly; use start() instead.
     * 
     * This can result in a few outcomes:
     * - a remote user is found immediately ("connected" event)
     * - Omegle likes us, but can't yet find a user ("waiting" event)
     * - various roadblocks from Omegle ("recaptchaRequired", "antinudeBanned", empty response, HTTP errors)
     */
    public String establishChat() {
        msgCount = 0;
        try {
            Util.retry(2, "Starting chat for " + getDisplayNick(), exception -> {
                String line = null;
                if (exception != null) {
                    dirtyFront(server);
                    server = chooseFront();
                    proxy.clearCheckCode();
                }
                if (!questionMode) {
                    logVerbose("!! Listening on front" + server);
                }
                URL url = new URL("https://front" + server + ".omegle.com/start?caps=recaptcha2,t3&firstevents=1&spid="
                        + "&randid=" + randid + (questionMode ? "&wantsspy=1" : getTopics())
                        + "&lang=" + lang
                        + "&cc=" + proxy.getCheckCode()
                    );
                logVerbose("Starting chat for " + getDisplayNick());
                proxy.use();
                URLConnection conn = url.openConnection(proxy.getProxy());
                conn.setDoOutput(true);
                fillHeaders(conn, "json post");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("");
                wr.flush();
                wr.close();
                try (Scanner s = new Scanner(conn.getInputStream());) {
                    line = s.useDelimiter(Util.EOF).next().trim();
                }

                //System.out.println("line=\""+line+"\"");
                if (line != null) {
                    if (line.trim().equals("")) {
                        banned("empty reply");
                    } else if (line.trim().equals("{}")) {
                        banned("empty object");
                    } else {
                        created = true;
                        lastActive = System.currentTimeMillis();
                        startSent();
                        handleReply(line);
                    }
                    //System.out.println("id: " + id);
                }
            });
        } catch (Exception ex) {
            connected = false;
            callback("died", ex.getMessage());
            dirtyFront(server);
            if (verbose) {
                ex.printStackTrace();
            } else {
                log(ex.toString());
            }
        }
        return id;
    }
    
    /** Hook to clean any state up after a ban */
    protected void banCleanup() {
        // nothing
    }
    
    /** Called when a ban is detected. Passes the type of ban to the callback. */
    protected void banned(String type) {
        banCleanup();
        callback("ban", type);
    }
    
    /** Hook to modify text before it's sent to the remote user */
    protected String spaceball(String in) {
        return in;
    }
    
    /**
     * Sends an action to Omegle. Blocking.
     * A message is required for "send" and "recaptcha" and forbidden for other actions.
     * This method splits messages over 2500 characters into pieces. Use sendActionInner to bypass this.
     */
    protected boolean sendAction(String action, String msg) {
        while (msg != null && msg.length() > 1500) {
            String left = msg.substring(0, msg.length() < 2500 ? 1000 : 1500);
            int seam = left.lastIndexOf("\nban ?off =");
            if (seam == -1)
                seam = left.lastIndexOf("\n");
            if (seam == -1)
                seam = left.lastIndexOf(". ") + 2;
            if (seam == 1)
                seam = left.lastIndexOf(" ") + 1;
            if (seam == 0 || seam < 300) {
                sendActionInner(action, left);
                msg = msg.substring(left.length());
            } else {
                sendActionInner(action, left.substring(0, seam));
                msg = msg.substring(seam);
            }
        }
        return sendActionInner(action, msg);
    }
    
    /** Returns the given message as a URL parameter */
    protected String formatMessage(String message) {
        return "msg=" + Util.urlEncode(spaceball(message));
    }
    
    /** Returns the given reCAPTCHA response as a URL parameter */
    protected String formatCaptchaResponse(String message) {
        return "response=" + Util.urlEncode(message);
    }
    
    /** Sends an action without splitting long messages. See sendAction. */
    protected boolean sendActionInner(String action, String msg) {
        return sendActionInner(action, msg, this::formatMessage);
    }

    /** Sends an action, using the given function to format the message as a URL parameter. */
    protected boolean sendActionInner(String action, String msg, UnaryOperator<String> formatter) {
        if (dummy) {
            if (action.equals("send")) {
                log(msg);
            } else {
                log("[Dummy-" + action + "]" + msg);
            }
            return true;
        }
        if (msgCount == -1 || id.isEmpty()) {
            return false;
        }
        try {
            Util.retry(2, "Sending " + action + " (" + msg + ") to " + getDisplayNick(), exception -> {
                URL url = new URL("https://front" + server + ".omegle.com/" + Util.urlEncode(action));
                java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection(effectiveProxy().getProxy());
                conn.setDoOutput(true);
                fillHeaders(conn, "post");
                OutputStreamWriter wr
                        = new OutputStreamWriter(conn.getOutputStream());
                if (msg != null) {
                    wr.write(formatter.apply(msg) + "&id=" + id);
                } else {
                    wr.write("id=" + id);
                }
                wr.flush();
                wr.close();
                BufferedReader rd;
                try {
                    rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } catch (java.io.FileNotFoundException e) {
                    rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                    log(getDisplayNick() + " UserConnection 404");
                    dirtyFront(server);
                }
                rd.readLine();
                rd.close();
            });
            return true;
        } catch (Exception ex) {
            log(getDisplayNick() + " had exception: " + ex.getMessage());
            dirtyFront(server);
            ex.printStackTrace();
            return false;
        }
    }
    
    /** Queues the given message to send to the remote user */
    public void schedSend(String message) {
        if (dummy)
            sendAction("send", message);
        else
            synchronized(tellQueue) {
                tellQueue.add(message);
            }
    }

    /** Prints the message to standard out with a timestamp */
    public static void log(Object message) {
        long time = System.currentTimeMillis();
        
        System.out.printf("%02d:%02d ", time / 60000 / 60 % 24, time / 60000 % 60);
        System.out.println(message);
    }

    /** If verbose is true, prints the message to standard out with a timestamp */
    public static void logVerbose(Object message) {
        if (verbose) {
            log(message);
        }
    }

    /** Queues the given message to send to the remote user, using the "system" template */
    public void schedTell(String message) {
        schedSend(t("system", null, message));
    }

    /** Immediately disconnects the remote user. Blocking. */
    public void sendDisconnect() {
        sendAction("disconnect", null);
    }
    
    /** Queues a disconnection of the remote user */
    public void schedSendDisconnect() {
        tellQueue.add("\u0091disconnect");
    }

    /** Sends this user's typing state. Blocking. */
    public void sendTyping(boolean typing) {
        if (typing)
            sendAction("typing", null);
        else
            sendAction("stoppedtyping", null);
    }

    /** Queues to send this user's typing state. */
    public void schedSendTyping(boolean typing) {
        if (typing)
            tellQueue.add("\u0091type");
        else
            tellQueue.add("\u0091stop");
    }
    
    /** Sends the given reCAPTCHA response and notifies the proxy of this. Blocking. */
    public void sendCaptcha(String response) {
        captchaSiteKey = null;
        proxy.captchaSubmitted();
        sendActionInner("recaptcha", response, this::formatCaptchaResponse);
    }
    
    /**
     * Ends the connection without explicitly disconnecting the remote user.
     * Omegle waits about two minutes after the last /events call before ending the session.
     */
    public void dispose() {
        done = true;
        tellQueue.add("\u0091end");
    }

    /**
     * Starts the connection.
     * This starts two threads. First, the remote poller is started, which connects to Omegle
     * and listens for events. On successful connection, the local poller is started to process
     * the queue of messages to send.
     */
    public void start() {
        if (remotePoller != null)
            throw new IllegalThreadStateException(this + " is already started");
        remotePoller = makeThread(this::run);
        remotePoller.start();
    }

    /**
     * Starts the connection and handles remote events in a loop.
     * This is run within the remote poller thread. Don't call this directly; use start() instead.
     */
    public void run() {
        if (id == null || id.isEmpty())
            id = establishChat();
        if (id != null) {
            startLocalPoller();
            handleEvents();
        }
    }

    /** Hook for overriding the Thread implementation used by this class. */
    protected Thread makeThread(Runnable task) {
        return Util.makeThread(task);
    }

    protected void startLocalPoller() {
        if (localPoller != null)
            throw new IllegalThreadStateException(this + ": local poller is already started");
        localPoller = makeThread(this::handleLocalEvents);
        localPoller.start();
    }
    
    /** Waits until the connection is closed. */
    public void join() throws InterruptedException {
        join(0);
    }

    /** Waits until the connection is closed or the given time elapses. */
    public void join(long millis) throws InterruptedException {
        if (remotePoller == null)
            return;
        remotePoller.join(millis);
    }

    /** Returns whether the session is alive (started and polling for events). */
    public boolean isAlive() {
        return remotePoller.isAlive();
    }

    /**
     * Handles local events in a loop.
     * This is run within the local poller thread. Don't call this directly; use start() instead.
     */
    protected void handleLocalEvents() {
        while (!done || !tellQueue.isEmpty()) {
            String message = "";
            try {
                message = tellQueue.take();
            } catch (InterruptedException e) {
                break;
            }
            if (!connected && !queueBeforeConnect()) {
                tellQueue.clear();
                continue;
            }
            handleLocalEvent(message);
        }
    }

    /**
     * Handles a single local event.
     * This can be a literal message or an action prefixed with \u0091.
     * The action can be "type", "stop", or "disconnect". Other actions are ignored.
     */
    protected void handleLocalEvent(String message) {
        if (message.equals("\u0091type")) {
            if (!seesTyping) {
                sendTyping(true);
                seesTyping = true;
            }
        } else if (message.equals("\u0091stop")) {
            if (tellQueue.isEmpty() || tellQueue.peek().startsWith("\u0091")) {
                sendTyping(false);
                seesTyping = false;
            }
        } else if (message.equals("\u0091disconnect")) {
            tellQueue.clear();
            sendDisconnect();
        } else if (message.startsWith("\u0091")) {
            logVerbose("[" + getDisplayNick() + " nop]" + message);
        } else {
            if (!message.startsWith("["))
                logVerbose("[" + getDisplayNick() + " is being told]" + message);
            sendAction("send", message);
            seesTyping = false;
        }
    }
    
    /**
     * Handles remote events in a loop.
     * This is run within the remote poller thread. Don't call this directly; use start() instead.
     */
    protected void handleEvents() {
        boolean[] rcvd = {true};
        while (rcvd[0] && !done) {
            try {
                Util.retry(6, "Getting events for " + getDisplayNick(), exception -> {
                    rcvd[0] = false;
                    if (exception != null) {
                        dirtyFront(server);
                        if (id.startsWith("central"))
                            server = chooseFront();
                    }
                    if (exception instanceof UnknownHostException) {
                        Util.sleep(10000);
                    }
                    URL url = new URL("https://front" + server + ".omegle.com/events");
                    URLConnection conn = url.openConnection(effectiveProxy().getProxy());
                    conn.setDoOutput(true);
                    fillHeaders(conn, "post json");
                    OutputStreamWriter wr
                            = new OutputStreamWriter(conn.getOutputStream());
                    wr.write("id=" + id);
                    wr.flush();
                    wr.close();
                    String line;
                    try (Scanner s = new Scanner(conn.getInputStream());) {
                        line = s.useDelimiter(Util.EOF).next().trim();
                    }
                    if (!line.equals("null")) {
                        rcvd[0] = true;
                        handleReply(line);
                    }
                });
            } catch (Exception ex) {
                connected = false;
                callback("died", ex.getMessage());
                dirtyFront(server);
                if (verbose) {
                    ex.printStackTrace();
                } else {
                    log(ex.toString());
                }
            }
        }
    }

    /**
     * Handles a single reply to /events.
     * This can have two forms:
     * - {"clientID": clientID, "events": [event, ...]}
     * - [event, ...]
     */
    protected void handleReply(String line) {
        if (line.isEmpty()) return;
        JsonValue top = JsonParser.parse(line);
        if (top.getKind() == Kind.OBJECT) {
            if (top.getMap().containsKey("clientID")) {
                id = top.getMap().get("clientID").toString();
            }
            if (top.getMap().containsKey("events")) {
                handleEventsReply(top.getMap().get("events").getList());
            }
        } else if (top.getKind() == Kind.ARRAY) {
            handleEventsReply(top.getList());
        } else {
            log(this + ": Can't handle reply: " + line);
        }
    }
    
    /**
     * Handles the events from a reply to /events.
     * An event is an array of the form [eventType, optionalArg, ...]
     */
    protected void handleEventsReply(List<JsonValue> events) {
        for (JsonValue event : events) {
            if (event.getKind() != Kind.ARRAY 
                    || event.getList().size() < 1 
                    || event.getList().get(0).getKind() != Kind.STRING) {
                log(this + ": Badly formed event: " + event);
                continue;
            }
            handleEvent(event.getList().get(0).toString(), 
                    event.getList().subList(1, event.getList().size()));
        }
    }
    
    /** Returns whether the list's first element is a JSON string */
    protected boolean expectStringArg(List<JsonValue> args) {
        return args.size() != 0 && args.get(0).getKind() == Kind.STRING;
    }
    
    /** Called when a remote user is found. Sets relevant fields and notifies the proxy and callback. */
    protected void connected() {
        if (!connected) {
            connected = true;
            lived = true;
            setBorn();
            lastActive = System.currentTimeMillis();
            proxy.undirty();
            proxy.use();
            callback("connected", null);
        }
    }
    
    /** Hook for when Omegle accepts our connection and starts to look for a remote user */
    protected void waiting() {}
    
    /** Records the time at which the chat started */
    protected void setBorn() {
        if (birth == Long.MAX_VALUE)
            birth = System.currentTimeMillis();
    }
    
    /** Called when a message is received. Sets lastLine and notifies the callback. */
    protected void gotMessage(String line) {
        lastLine = line;
        callback("message", line);
    }
    
    /** Hook for handling common likes reported by Omegle at the start of the chat */
    protected void hasCommonLikes(String commonLikes) {}
    
    /**
     * Handles a single event from a reply to /events.
     * Most events, in the end, result in a call of the callback.
     */
    protected void handleEvent(String event, List<JsonValue> args) {
        if (event.equals("recaptchaRequired") || event.equals("recaptchaRejected")) {
            banCleanup();
            if (!expectStringArg(args))
                log(this + ": Captcha challenge missing:" + args);
            else
                callback("captcha", captchaSiteKey = args.get(0).toString());
        } else if (event.equals("error")) {
            String error = expectStringArg(args) ? args.get(0).toString() : "(unknown)";
            log(this + ": user-facing error: " + error);
            proxy.clearCheckCode();
        } else if (event.equals("antinudeBanned")) {
            banned("antinudeBanned");
        } else if (event.equals("typing")) {
            lastActive = System.currentTimeMillis();
            callback("typing", null);
            typing = true;
        } else if (event.equals("stoppedTyping")) {
            lastActive = System.currentTimeMillis();
            callback("stoppedtyping", null);
            typing = false;
        } else if (event.equals("question")) {
            if (!expectStringArg(args))
                log(this + ": Question missing: " + args);
            else
                question = args.get(0).toString();
            connected();
        } else if (event.equals("gotMessage")) {
            connected();
            lastActive = System.currentTimeMillis();
            if (!expectStringArg(args)) {
                log(this + ": Message missing: " + args);
            } else {
                gotMessage(args.get(0).toString());
            }
            typing = false;
            msgCount++;
        } else if (event.equals("strangerDisconnected")) {
            connected = false;
            callback("disconnected", null);
            dispose();
            msgCount = -1;
        } else if (event.equals("connected") && !questionMode) {
            connected();
        } else if (event.equals("commonLikes")) {
            if (args.size() == 0 || args.get(0).getKind() != Kind.ARRAY) {
                log(this + ": Common likes missing: " + args);
            } else {
                commonLikes = args.get(0).getList().stream().map(Object::toString).toArray(String[]::new);
                if (commonLikes.length > 0) {
                    commonLikesFlat = String.join(", ", commonLikes);
                    hasCommonLikes(commonLikesFlat);
                }
            }
        } else if (event.equals("waiting")) {
            waiting();
        }
    }
    
    private static final JsonSerializer<UserConnection> serializer = 
            new JsonSerializer<UserConnection>(UserConnection.class) {
        @Override
        void customSave(UserConnection t, Map<String, Object> map) {
            if (t == null) return;
            // ProxyConfig objects live in the serialization of Proxies.
            // Here, we only save the proxy's address.
            // Setting the proxy again when deserializing has to be done by the client.
            map.put("proxy", t.proxy.saveAddress());
        }
    };
    
    public JsonValue killAndSave() {
        if (!dummy)
            dispose();
        return serializer.save(this);
    }
    
    public UserConnection load(JsonValue spec) {
        serializer.load(this, spec);
        if (connected) created = true;
        return this;
    }
    
    public static JsonValue saveStatic() {
        return serializer.save(null);
    }
    
    public static void loadStatic(JsonValue spec) {
        serializer.load(null, spec);
    }
    
    /**
     * An Omegle front server, through which all of the chat data is sent.
     * There are a few of these, but some of them are usually down.
     * If one server doesn't work, we dirty it for 10 minutes and switch to another one.
     */
    private static class Front {
        final int id;
        long lastDirty;
        Front(int id) { this.id = id + 1; } // Arrays:setAll gives us 0-based indices, but the subdomains start at 1
        void dirty() { lastDirty = System.currentTimeMillis(); }
        boolean isDirty() {
            return System.currentTimeMillis() - lastDirty < 10 * 60 * 1000;
        }
    }
    
    private static Front[] fronts = new Front[48];
    private static Comparator<Front> frontComparator = Comparator.comparingLong(a -> a.lastDirty);
    static {
        Arrays.setAll(fronts, Front::new);
    }
    
    public static int chooseFront() {
        synchronized (fronts) {
            Front front = Util.randomize(fronts);
            if (front.isDirty()) {
                Arrays.sort(fronts, frontComparator);
                front = fronts[0];
            }
            return front.id;
        }
    }
    
    public static void dirtyFront(int id) {
        synchronized (fronts) {
            Arrays.stream(fronts)
                .filter(a -> a.id == id)
                .findFirst()
                .ifPresent(Front::dirty);
        }
    }
    
}
