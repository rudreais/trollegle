package anon.trollegle;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;


/**
 * A proxy address (java.net.Proxy) together with Omegle-specific data such as bans.
 */
public class ProxyConfig implements Comparable<ProxyConfig> {
    
    /** Two captchas received within this time are treated as a captcha ban */
    public static final long MIN_CAPTCHA_GRACE = 1000 * 60 * 15;
    /** Time for which to give up on a proxy once a ban is detected */
    public static final long BAN_LENGTH = 1000 * 60 * 60 * 24;
    /** Time for which to give up on a proxy once a non-ban error occurs */
    public static final long DIRTY_LENGTH = 1000 * 60 * 7;
    public static final int NUM_RANDIDS = 4;
    public static final int PING_INTERVAL = 300000;
    /** External site used to record exit IPs, to avoid using multiple logical proxies with the same IP */
    public static final String IP_CHECK_SITE = "http://api.ipify.org/";
    /** Used for the check code (cc) required when opening a chat */
    public static final String[] ANTINUDE_SERVERS = {
        "waw1.omegle.com",
        "waw2.omegle.com",
        "waw3.omegle.com",
        "waw4.omegle.com",
    };
    
    /** The proxy address and type */
    private Proxy proxy;
    /** If the proxy is a Tor instance, its control port */
    private InetSocketAddress torControl;
    /** If the proxy is a Tor instance, a thread used to look for working exit nodes */
    private TorSearch torSearch;
    
    @Persistent
    private long lastCaptcha, lastCaptchaSubmit, lastBan, lastUse, lastDirty, lastPing;
    @Persistent
    private boolean enabled = true;
    
    /** A way to mark certain proxies as "last resorts". Proxies of lower priorities are ignored as long
     * as proxies of higher priorities are available. */
    @Persistent
    private int priority;
    private String[] randids;
    @Persistent
    String exitAddress = "";
    @Persistent
    private String checkCode;
    @Persistent
    private long lastCheckCode;
    
    public ProxyConfig(Proxy proxy) {
        this.proxy = proxy;
        randids = new String[(int) (NUM_RANDIDS / 2 + Math.random() * NUM_RANDIDS)];
        for (int i = 0; i < randids.length; i++) {
            randids[i] = Util.makeRandid();
        }
    }
    
    public Proxy getProxy() { return proxy; }
    
    /** Marks the proxy as captcha'd, or if the captcha grace hasn't passed, as captcha banned */
    public void captcha() {
        long time = System.currentTimeMillis();
        lastDirty = time;
        if (time - lastCaptchaSubmit < MIN_CAPTCHA_GRACE)
            lastBan = time;
        lastCaptcha = time;
    }
    /** Records a captcha submission */
    public void captchaSubmitted() {
        lastCaptcha = 0;
        lastBan = 0;
        lastDirty = 0;
        lastCaptchaSubmit = System.currentTimeMillis();
    }
    /** Marks the proxy as (hard) banned */
    public void ban() {
        lastCaptcha = 0;
        lastBan = System.currentTimeMillis();
    }
    /** Marks the proxy as dirty */
    public void dirty() {
        lastDirty = System.currentTimeMillis();
    }
    /** Resets the proxy's captcha, ban, and dirty state */
    public void unban() {
        lastCaptcha = 0;
        lastCaptchaSubmit = 0;
        lastBan = 0;
        lastDirty = 0;
    }
    /** Resets the dirty state */
    public void undirty() {
        lastDirty = 0;
    }
    
    /**
     * Returns a check code for this proxy.
     * This blocks if there is no check code yet, or we think it has expired (i.e. two hours have
     * passed since the full hour before getting it). Sloppy, but kind of okay because we only call
     * it from UserConnection in its own thread which can't do anything until it has the code.
     */
    public String getCheckCode() {
        if (checkCode == null || checkCodeExpired()) {
            checkCode = fetchCheckCode();
            lastCheckCode = System.currentTimeMillis();
        }
        return checkCode;
    }
    protected boolean checkCodeExpired() {
        long fullHourBeforeLast = lastCheckCode / 1000 / 60 / 60 * 60 * 60 * 1000;
        return System.currentTimeMillis() - fullHourBeforeLast > 2 * 60 * 60 * 1000;
    }
    protected String fetchCheckCode() {
        try {
            URL url = new URL("https://" + Util.randomize(ANTINUDE_SERVERS) + "/check");
            URLConnection conn = url.openConnection(proxy);
            Util.fillHeaders(conn, "post");
            conn.setDoOutput(true);
            conn.getOutputStream().close();
            try (Scanner s = new Scanner(conn.getInputStream());) {
                return s.useDelimiter(Util.EOF).next();
            }
        } catch (Exception e) {
            throw e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e);
        }
    }
    public void clearCheckCode() {
        checkCode = null;
    }
    
    /** Gets one of the proxy's randids at random */
    public String getRandid() {
        return Util.randomize(randids);
    }
    
    /** Gets the proxy's external IP address using the IP check site */
    public String checkExitAddress() {
        String line;
        try {
            URL url = new URL(IP_CHECK_SITE);
            URLConnection conn = url.openConnection(proxy);
            try (Scanner s = new Scanner(conn.getInputStream());) {
                return s.useDelimiter(Util.EOF).next();
            }
        } catch (Exception ex) {
            UserConnection.log(this + " exit ip check: " + ex);
            return null;
        }
    }
    
    /**
     * Gets the proxy's external IP address.
     * Returns the cached address if present, otherwise sets it using the IP check site.
     */
    public String updateAndGetExitAddress() {
        if (exitAddress.isEmpty()) {
            String newAddress = checkExitAddress();
            if (newAddress != null)
                exitAddress = newAddress;
        }
        return exitAddress;
    }
    
    /** Gets the cached external address, if any. */
    public String getExitAddress() {
        return exitAddress;
    }
    
    /** Updates the last use time */
    public void use() {
        if (!isSearchingTor())
            lastUse = System.currentTimeMillis();
    }
    
    public boolean isEnabled() { return enabled; }
    public void setEnabled(boolean enabled) { this.enabled = enabled; }
    
    public int getPriority() { return priority; }
    public void setPriority(int priority) { this.priority = priority; }
    
    public boolean isTor() { return torControl != null; }
    public InetSocketAddress getTorControl() { return torControl; }
    public void setTorControl(InetSocketAddress torControl) { this.torControl = torControl; }
    
    /**
     * Abandons the current Tor exit node and starts a search for a new one.
     * The callback is called when the search succeeds or fails. See TorSearch.
     */
    public synchronized void startTorSearch(Callback callback) {
        if (isSearchingTor())
            return;
        if (proxy == Proxy.NO_PROXY)
            throw new UnsupportedOperationException();
        torSearch = new TorSearch(this, callback);
        exitAddress = "";
        torSearch.start();
    }
    public boolean isSearchingTor() {
        return torSearch != null && torSearch.isAlive();
    }
    
    /** Returns whether the proxy is captcha'd but possibly not banned. */
    public boolean isFirstCaptcha() {
        return lastCaptcha > lastUse && lastCaptcha == lastDirty && !isBanned();
    }
    
    public boolean isBanned() {
        return System.currentTimeMillis() - lastBan < BAN_LENGTH;
    }
    
    public boolean isDirty() {
        return System.currentTimeMillis() - lastDirty < DIRTY_LENGTH;
    }
    
    /** Returns whether it's safe to use the proxy (enabled, not banned, not searching) */
    public boolean canUse() {
        return enabled && !isBanned() && (!isSearchingTor() || torSearch.isAccepted());
    }
    
    /** Comparison function. "Smallest" proxies are most usable. */
    public int compareTo(ProxyConfig other) {
        if (this.canUse() && !other.canUse())
            return 1;
        if (!this.canUse() && other.canUse())
            return -1;
        if (!this.isDirty() && other.isDirty())
            return 1;
        if (this.isDirty() && !other.isDirty())
            return -1;
        if (other.priority < this.priority)
            return 1;
        if (other.priority > this.priority)
            return -1;
        return (int) Math.signum(other.lastUse - this.lastUse);
    }
    
    public String toString() {
        String thing = proxy.toString();
        if (priority != 0)
            thing += " (priority " + priority + ")";
        if (!enabled)
            thing += " (disabled)";
        if (isBanned())
            thing += " (banned " + Util.minSec(System.currentTimeMillis() - lastBan) + " ago)";
        else if (isFirstCaptcha())
            thing += " (captcha'd " + Util.minSec(System.currentTimeMillis() - lastCaptcha) + " ago)";
        else if (isDirty())
            thing += " (marked dirty " + Util.minSec(System.currentTimeMillis() - lastDirty) + " ago)";
        else if (lastUse != 0)
            thing += " (last used " + Util.minSec(System.currentTimeMillis() - lastUse) + " ago)";
        if (isSearchingTor())
            thing += " (searching)";
        return thing;
    }
    
    public boolean shouldPing() {
        return System.currentTimeMillis() - PING_INTERVAL >= lastPing;
    }
    
    /**
     * Sends a ping to Omegle's /status endpoint.
     * This does nothing, as far as I can tell, but may be useful for proactively disabling bad fronts
     * (see UserConnection)
     */
    public void pingStatus() {
        if (!shouldPing()) return;
        lastPing = System.currentTimeMillis();
        String randid = randids[(int) (Math.random() * randids.length)];
        int server = UserConnection.chooseFront();
        String description = randid + " on " + this + " front " + server;
        try {
            URL homeUrl = new URL("https://front" + server + ".omegle.com/status?nocache=" + Math.random() + "&randid=" + randid);
            URLConnection conn = homeUrl.openConnection(proxy);
            Util.fillHeaders(conn, "json");
            try (Scanner s = new Scanner(conn.getInputStream());) {
                String line = s.useDelimiter(Util.EOF).next().trim();
                UserConnection.logVerbose("Status for " + description + ": " + line);
            }
        } catch (Exception ex) {
            UserConnection.log("Failed to get status for " + description);
            if (UserConnection.verbose) {
                ex.printStackTrace();
            }
            UserConnection.dirtyFront(server);
        }

    }
    
    private final Pattern removeIP = Pattern.compile("/[^/:]+");
    
    /**
     * Matches this object's toString() representation, optionally mangled, against a mangled regex.
     * The toString() string is mangled in three ways: removing the IP, replacing ':' with ' ', and both at once.
     * The regex is surrounded with "\\b" and any "." are replaced with "\\.".
     */
    public boolean matches(String regex) {
        Pattern pattern = Pattern.compile("\\b" + regex.replaceAll("(?<!\\\\)\\.", "\\.") + "\\b");
        String full = toString();
        String noIP = removeIP.matcher(full).replaceAll("");
        return pattern.matcher(full).find()
            || pattern.matcher(noIP).find()
            || pattern.matcher(full.replace(":", " ")).find()
            || pattern.matcher(noIP.replace(":", " ")).find();
    }
    
    // Helpers for serializing the IP addresses to JSON
    String[] saveAddress() {
        return saveAddress((InetSocketAddress) proxy.address());
    }
    
    static String[] saveAddress(InetSocketAddress address) {
        if (address == null)
            return null;
        return new String[] {address.getHostString(), String.valueOf(address.getPort())};
    }
    
    static InetSocketAddress loadAddress(String[] spec) {
        return new InetSocketAddress(spec[0], Integer.parseInt(spec[1]));
    }
    
    private static final JsonSerializer<ProxyConfig> serializer = 
            new JsonSerializer<ProxyConfig>(ProxyConfig.class) {
        @Override
        void customSave(ProxyConfig t, Map<String, Object> map) {
            if (t == null) return;
            map.put("type", t.proxy.type().name());
            map.put("address", t.saveAddress());
            map.put("torControl", saveAddress(t.torControl));
            map.put("isSearchingTor", t.isSearchingTor());
        }
        @Override
        void customLoad(ProxyConfig t, Map<String, JsonValue> map) {
            if (t == null) return;
            JsonValue type = map.get("type"),
                address = map.get("address"),
                torControl = map.get("torControl"),
                isSearchingTor = map.get("isSearchingTor");
                
            if (!JsonValue.isNull(type) && !type.toString().equals("DIRECT")
                    && !JsonValue.isNull(address))
                t.proxy = new Proxy(Proxy.Type.valueOf(type.toString()),
                        loadAddress(address.getAs(String[].class)));
            if (!JsonValue.isNull(torControl))
                t.torControl = loadAddress(torControl.getAs(String[].class));
            if (!JsonValue.isNull(isSearchingTor) && isSearchingTor.getBoolean())
                t.dirty();
        }
    };
    
    public JsonValue save() {
        return serializer.save(this);
    }
    
    public ProxyConfig load(JsonValue spec) {
        return serializer.load(this, spec);
    }
    
}
